﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class Team : Entity
    {
        public string Name { get; set; }
        public ICollection<Project> Projects { get; set; }
        public ICollection<User> Users { get; set; }
        public DateTime CreatedAt { get; set; }

        public Team()
        {
            Projects = new List<Project>();
            Users = new List<User>();
        }
    }
}
