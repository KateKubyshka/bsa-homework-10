﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LinqPracticeLesson1
{
    public static class HttpManager 
    {
        private static readonly string uri = "https://localhost:44368/api/";

        private static async Task<T> GetData<T>(string path)
        {
            using HttpClient httpClient = new();
            HttpResponseMessage response = await httpClient.GetAsync(uri + path);
            response.EnsureSuccessStatusCode();
            return JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync());
        }
        public static async Task<IEnumerable<Project>> GetProjects() => await GetData<IEnumerable<Project>>("projects");
        public static async Task<Project> GetProjectById(int id) => await GetData<Project>($"projects/{id}");

        

        public static async Task<IEnumerable<ProjectTask>> GetProjectTasks()
        {
            using HttpClient httpClient = new();
            HttpResponseMessage response = await httpClient.GetAsync(uri + "tasks");
            response.EnsureSuccessStatusCode();
            return JsonConvert.DeserializeObject<IEnumerable<ProjectTask>>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<ProjectTask> GetProjectTaskById(int id)
        {
            using HttpClient httpClient = new();
            HttpResponseMessage response = await httpClient.GetAsync(uri + $"tasks/{id}");
            response.EnsureSuccessStatusCode();
            return JsonConvert.DeserializeObject<ProjectTask>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<IEnumerable<Team>> GetTeams()
        {
            using HttpClient httpClient = new();
            HttpResponseMessage response = await httpClient.GetAsync(uri + "teams");
            response.EnsureSuccessStatusCode();
            return JsonConvert.DeserializeObject<IEnumerable<Team>>(await response.Content.ReadAsStringAsync());
        }
        public static async Task<Team> GetTeamById(int id)
        {
            using HttpClient httpClient = new();
            HttpResponseMessage response = await httpClient.GetAsync(uri + $"teams/{id}");
            response.EnsureSuccessStatusCode();
            return JsonConvert.DeserializeObject<Team>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<IEnumerable<User>> GetUsers()
        {
            using HttpClient httpClient = new();
            HttpResponseMessage response = await httpClient.GetAsync(uri + "users");
            response.EnsureSuccessStatusCode();
            return JsonConvert.DeserializeObject<IEnumerable<User>>(await response.Content.ReadAsStringAsync());
        }
        public static async Task<User> GetUserById(int id)
        {
            using HttpClient httpClient = new();
            HttpResponseMessage response = await httpClient.GetAsync(uri + $"users/{id}");
            response.EnsureSuccessStatusCode();
            return JsonConvert.DeserializeObject<User>(await response.Content.ReadAsStringAsync());
        }
    }
}
