﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using WebAPI.Interfaces;

namespace WebAPI.Parsers
{
    public class Parser : IParser
    {
        public IEnumerable<T> Parse<T>(string path)
        {
            string json = File.ReadAllText(path);
            return JsonConvert.DeserializeObject<IEnumerable<T>>(json);
        }
    }
}
