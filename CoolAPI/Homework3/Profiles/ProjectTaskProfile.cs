﻿using AutoMapper;
using DAL.Entities;
using WebAPI.DTOs;

namespace WebAPI.Profiles
{
    public class ProjectTaskProfile : Profile
    {
        public ProjectTaskProfile()
        {
            CreateMap<ProjectTask, ProjectTaskDTO>();
            CreateMap<ProjectTaskDTO, ProjectTask>();
        }
    }
}
